package com.getjavajob.training.algo.init.avdonino;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String args[]) {
        System.out.print("Enter a number: ");
        Scanner keyboardInput = new Scanner(System.in);
        int number = keyboardInput.nextInt();
        System.out.println("You have entered number: " + number);
    }
}
